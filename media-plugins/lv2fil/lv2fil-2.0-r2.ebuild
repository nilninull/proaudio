# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6

PYTHON_COMPAT=( python2_7 )
inherit toolchain-funcs python-r1

REQUIRED_USE="${PYTHON_REQUIRED_USE}"

DESCRIPTION="Four-band parametric equaliser LV2 plugin"
HOMEPAGE="http://nedko.arnaudov.name/soft/lv2fil/trac/"
SRC_URI="http://nedko.arnaudov.name/soft/lv2fil/download/${P}.tar.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""

RESTRICT="mirror"

DOCS=( AUTHORS NEWS README )

DEPEND=">=media-libs/lv2-1.0.0[${PYTHON_USEDEP}]"
RDEPEND="${DEPEND}
	${PYTHON_DEPS}
	>=dev-python/pycairo-1.8.8[${PYTHON_USEDEP}]
	>=dev-python/pygtk-2.16.0-r1:2[${PYTHON_USEDEP}]
	>=media-libs/pyliblo-0.8.1[${PYTHON_USEDEP}]"

src_configure() {
	python_setup

	tc-export CC CPP CXX AR RANLIB
	./waf configure --lv2-dir=/usr/$(get_libdir)/lv2 \
		|| die "waf configure failed"
}

src_compile() {
	./waf build || die "waf build failed"
}

src_install() {
	./waf --destdir="${D}" install || die "waf install failed"
	dodoc AUTHORS NEWS README
}
