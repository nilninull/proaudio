# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

KEYWORDS="-* ~amd64 ~x86"
MULTILIB_COMPAT=( abi_x86_{32,64} )
inherit multilib-minimal

DESCRIPTION="ASIO driver for WINE"
HOMEPAGE="http://sourceforge.net/projects/wineasio"
SRC_URI="mirror://sourceforge/${PN}/${PN}/${P}.tar.gz"
RESTRICT="mirror"

LICENSE="GPL-2"
SLOT="0"
IUSE=""

DEPEND="media-libs/asio-sdk"
RDEPEND="
	app-emulation/wine[${MULTILIB_USEDEP}]
	virtual/jack[${MULTILIB_USEDEP}]
"

S="${WORKDIR}/${PN}"

src_prepare() {
	cp /opt/asiosdk2.3/common/asio.h .
	default
	multilib_copy_sources
}

multilib_src_compile() {
	if [[ "${ABI}" = "amd64" ]]; then
		/bin/bash prepare_64bit_asio
		mv Makefile64 Makefile || die "mv failed"
	fi
	default
}

multilib_src_install() {
	exeinto /usr/$(get_abi_LIBDIR)/wine
	doexe *.so
	dodoc README
}

pkg_postinst() {
	echo
	elog "You need to register the DLL by typing"
	elog
	elog "regsvr32 wineasio.dll"
	elog
	elog "AS THE USER who uses wine!"
	elog "Then open winecfg -> Audio -> and enable ONLY the ALSA driver!"
	echo
}
