# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

MY_PN="${PN/-/}"
MY_P="${MY_PN}${PV}"
At="${MY_P}.zip"

DESCRIPTION="Steinberg ASIO SDK 2.3 - win32"
HOMEPAGE="http://www.steinberg.net/en/company/developers.html"
SRC_URI="${At}"

RESTRICT="fetch"

LICENSE="STEINBERG_SOFT-UND_HARDWARE_GMBH"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""

DEPEND="app-arch/unzip"
RDEPEND=""

S="${WORKDIR}/ASIOSDK2.3"

pkg_nofetch() {
	einfo "1. go to ${HOMEPAGE}"
	einfo "2. read the license agreement"
	einfo "3. if you accept it, then download ${At}"
	einfo "4. move ${At} to ${DISTDIR}"
	einfo
}

src_install() {
	insinto "/opt/${MY_P}"
	doins -r .
}

pkg_postinst() {
	echo
	elog "${P} has been installed to /opt/${MY_P}"
	elog "To re-read the license please refer to"
	elog "/opt/${MY_P}/Steinberg ASIO Licensing Agreement.pdf"
	echo
}
