# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6

inherit eutils subversion

DESCRIPTION="uCApps.de MIOS Studio"
HOMEPAGE="http://ucapps.de/"

ESVN_REPO_URI="svn://svnmios.midibox.org/mios32/trunk/tools"
ESVN_PROJECT="mios32_tools"

LICENSE="TAPR-NCL"
KEYWORDS="-* ~amd64 ~x86"
SLOT="0"
IUSE="debug"

RESTRICT="strip mirror"

RDEPEND="app-arch/bzip2
	dev-libs/libbsd
	media-libs/alsa-lib
	media-libs/freetype
	media-libs/libpng:0
	sys-libs/zlib
	x11-libs/libX11
	x11-libs/libXau
	x11-libs/libXdmcp
	x11-libs/libXext
	x11-libs/libxcb
"
DEPEND="${RDEPEND}
	app-arch/unzip"

S="${WORKDIR}"

src_prepare() {
	cd "${S}/juce"
	unzip unpack_me.zip || die
}

src_compile() {
	local myconf=""
	use debug && myconf="CONFIG=Debug" || myconf="CONFIG=Release"

	cd "${S}/mios_studio/Builds/Linux"
	emake "${myconf}"
}

src_install() {
	cd "${S}/mios_studio/Builds/Linux/build"
	exeinto /usr/bin
	doexe MIOS_Studio
}
