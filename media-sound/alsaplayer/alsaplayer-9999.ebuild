# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit autotools git-r3

EGIT_REPO_URI="git://github.com/alsaplayer/alsaplayer.git"

DESCRIPTION="Media player primarily utilising ALSA"
HOMEPAGE="http://www.alsaplayer.org/"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS=""
IUSE="alsa doc flac gtk jack mad mikmod nas nls ogg opengl oss vorbis systray xosd"

REQUIRED_USE="systray? ( gtk )"

RDEPEND="media-libs/libsndfile
	alsa? ( media-libs/alsa-lib )
	mad? ( media-libs/libmad )
	flac? ( media-libs/flac )
	jack? ( virtual/jack )
	mikmod? ( media-libs/libmikmod )
	nas? ( media-libs/nas )
	ogg? ( media-libs/libogg )
	opengl? ( virtual/opengl )
	vorbis? ( media-libs/libvorbis )
	xosd? ( x11-libs/xosd )"

DEPEND="${RDEPEND}
	>=dev-libs/glib-2.10.1
	virtual/pkgconfig
	doc? ( app-doc/doxygen )
	nls? ( sys-devel/gettext )
	gtk? ( x11-libs/gtk+:2 )"

DOCS=( AUTHORS ChangeLog README.md TODO docs/wishlist.txt )

src_prepare() {
	eautoreconf
	default
}

src_configure() {
	use xosd || export ac_cv_lib_xosd_xosd_create="no"

	use doc || export ac_cv_prog_HAVE_DOXYGEN="false"

	local myeconfargs=(
		$(use_enable flac)
		$(use_enable jack)
		$(use_enable mad)
		$(use_enable mikmod)
		$(use_enable nas)
		$(use_enable opengl)
		$(use_enable oss)
		$(use_enable nls)
		$(use_enable sparc)
		$(use_enable vorbis oggvorbis)
		$(use_enable gtk gtk2)
		$(use_enable systray)
		--disable-esd
		--disable-sgi
		--disable-dependency-tracking
	)
	default
}
