# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI="5"
inherit cmake-utils fdo-mime gnome2-utils

DESCRIPTION="MIDI and audio sequencer and notation editor."
HOMEPAGE="http://www.rosegardenmusic.com/"

if [[ ${PV} == 9999 ]]; then
	ESVN_REPO_URI="http://svn.code.sf.net/p/${PN}/code/trunk/${PN}"
	SRC_URI=""
	inherit subversion
else
	SRC_URI="mirror://sourceforge/${PN}/${P}.tar.bz2"
fi

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86 ~amd64 ~ppc"
IUSE="debug alsa jack sndfile lirc qt4 qt5 lilypond"

RDEPEND="qt4? ( dev-qt/qtcore:4 dev-qt/qtgui:4 )
	qt5? ( dev-qt/qtcore:5 dev-qt/qtgui:5 )
	>=media-libs/ladspa-cmt-1.14
	>=media-libs/ladspa-sdk-1.1
	>=media-libs/liblo-0.23[threads(+)]
	media-libs/liblrdf
	>=media-libs/libsamplerate-0.1.2
	sci-libs/fftw:3.0
	x11-misc/makedepend
	|| ( x11-libs/libX11 virtual/x11 )
	>=media-libs/dssi-0.9
	jack? ( virtual/jack )
	alsa? ( >=media-libs/alsa-lib-1.0 )
	sndfile? ( >=media-libs/libsndfile-1.0.16 )
	lirc? ( >=app-misc/lirc-0.8 )
	lilypond? ( >=media-sound/lilypond-2.6.0 )"

DEPEND="${RDEPEND}
	virtual/pkgconfig"

REQUIRED_USE="^^ ( qt4 qt5 )"

PATCHES=( "${FILESDIR}"/${PN}-16.02-cmake-deps.patch )

src_prepare() {
	if [[ ${PV} == 9999 ]]; then
		subversion_src_prepare
	fi
	cmake-utils_src_prepare
}

src_configure() {
	local mycmakeargs=(
		$(cmake-utils_use_enable alsa ALSA)
		$(cmake-utils_use_enable jack JACK)
		$(cmake-utils_use_enable sndfile SNDFILE)
		$(cmake-utils_use_disable lirc LIRC)
		$(cmake-utils_use_use qt4 QT4)
		$(cmake-utils_use_use qt5 QT5)
	)

	# Gentoo redefines CMAKE_BUILD_TYPE
	if use debug ; then
		sed -i \
		-e 's/NOT CMAKE_BUILD_TYPE STREQUAL "Debug"/CMAKE_BUILD_TYPE STREQUAL "Release"/' \
		-e 's/CMAKE_BUILD_TYPE STREQUAL "Debug"/NOT CMAKE_BUILD_TYPE STREQUAL "Release"/' \
                CMakeLists.txt || die
        fi

	cmake-utils_src_configure
}

pkg_preinst() {
	gnome2_icon_savelist
}

pkg_postinst() {
	gnome2_icon_cache_update
	fdo-mime_desktop_database_update
	fdo-mime_mime_database_update
}

pkg_postrm() {
	gnome2_icon_cache_update
	fdo-mime_desktop_database_update
	fdo-mime_mime_database_update
}
